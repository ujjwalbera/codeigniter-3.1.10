<?php
/**
 * Created by PhpStorm.
 * User: ujjwalbera
 * Date: 3/2/19
 * Time: 11:19 AM
 */

class Myclfirst  extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->output->set_template('default');

		$this->output->js('assets/themes/default/js/jquery-1.9.1.min.js');
		$this->output->js('assets/themes/default/hero_files/bootstrap-transition.js');
		$this->output->js('assets/themes/default/hero_files/bootstrap-collapse.js');
	}

	public function index()
	{
		$this->load->view('ci_simplicity/welcome');
	}

	public function example_1()
	{
		$this->load->view('ci_simplicity/example_1');
	}

	public function example_2()
	{
		$this->output->set_template('simple');
		$this->load->view('ci_simplicity/example_2');
	}

	public function example_3()
	{
		$this->output->section('sidebar', 'ci_simplicity/sidebar');
		$this->load->view('ci_simplicity/example_3');
	}

	public function example_4()
	{
		$this->output->unset_template();
		$this->load->view('ci_simplicity/example_4');
	}


}
