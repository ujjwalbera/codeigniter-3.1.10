<?php
 
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Model extends CI_Model {
 
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->form_validation->CI =& $this; 
    }

    
 
}
 
/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
